<!DOCTYPE HTML>
<html>
    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Winter Hack</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,700,800" rel="stylesheet">
    
    <!-- Animate.css -->
    <link rel="stylesheet" href="{{asset('css/animate.css')}}">
    <!-- Icomoon Icon Fonts-->
    <link rel="stylesheet" href="{{asset('css/icomoon.css')}}">
    <!-- Bootstrap  -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap-switch.min.css')}}">
    <!-- Magnific Popup -->
    <link rel="stylesheet" href="{{asset('css/magnific-popup.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('sweetalert/sweetalert.css')}}">
    <!-- Owl Carousel  -->
    <link rel="stylesheet" href="{{asset('css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/owl.theme.default.min.css')}}">

    <!-- Theme style  -->
    <link rel="stylesheet" href="{{asset('css/style.css')}}">

    <!-- Modernizr JS -->
    <script src="{{asset('js/modernizr-2.6.2.min.js')}}"></script>
    <!-- FOR IE9 below -->
    <!--[if lt IE 9]>
    <script src="js/respond.min.js"></script>
    <![endif]-->

    </head>
    <body>
        
    <div class="fh5co-loader"></div>
    
    <div id="page">
    <nav class="fh5co-nav" role="navigation">
        <div class="top">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 text-right">
                        <p class="num">Call: +52 614 216 3785</p>
                        <ul class="fh5co-social">
                            <li><a href="https://www.facebook.com/events/1381642831875722/"><i class="icon-facebook"></i></a></li>
                            <li><a href="#"><i class="icon-dribbble"></i></a></li>
                            <li><a href="#"><i class="icon-github"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>      
        </div>
        <div class="top-menu">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <div id="fh5co-logo"><a href="">HackSpirits<span>.</span></a></div>
                    </div>
                    <div class="col-xs-8 text-right menu-1">
                        <ul>
                            <li><a href=""">Inicio</a></li>
                            <li><a href="{{route('participantes.index')}}">Registrados</a></li>
                            <li><a href="{{route('workshop.index')}}">Workshop</a></li>
                        </ul>
                    </div>
                </div>
                
            </div>
        </div>
    </nav>

    <!--<header id="fh5co-header" class="fh5co-cover fh5co-cover-sm" role="banner" style="background-image:url(images/event_logo.jpg);" data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <div class="display-t">
                        <div class="display-tc animate-box" data-animate-effect="fadeIn">
                            <h1>Registrate</h1>
                            <h2>Free html5 templates Made by <a href="http://freehtml5.co" target="_blank">freehtml5.co</a></h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>-->
    <!--<div id="map" class="fh5co-map"></div>-->
        <!-- END map -->
    
    <div id="fh5co-contact">
        <div class="container">
            <div class="row">
                <!--<div class="col-md-5 col-md-push-1 animate-box">
                    
                    <div class="fh5co-contact-info">
                        <h3>Contact Information</h3>
                        <ul>
                            <li class="address">198 West 21th Street, <br> Suite 721 New York NY 10016</li>
                            <li class="phone"><a href="tel://1234567920">+ 1235 2355 98</a></li>
                            <li class="email"><a href="mailto:info@yoursite.com">info@yoursite.com</a></li>
                            <li class="url"><a href="http://gettemplates.co">gettemplates.co</a></li>
                        </ul>
                    </div>

                </div>-->
                <div class="col-md-11 animate-box">
                    <h3></h3>
                     {!! Form::open(['route' => 'workshop.store', 'method' => 'POST' ]) !!}
                        <div class="row form-group">
                            <div class="col-md-6">
                                <label for="title">Workshop</label>
                                <input type="text" id="title" name="title" class="form-control" placeholder="Ingresa el nombre del workshop">
                            </div>
                            <div class="col-md-6">
                                <label for="author">Expositor</label>
                                <input type="text" id="author" name="author" class="form-control" placeholder="quien dara el workshop?">
                            </div>
                        </div>
                        <div class="form-group pull-right">
                            <input type="submit" value="Registrar" class="btn btn-primary">
                        </div>
                    {!! Form::close() !!}     
                </div>
            </div>
            
        </div>
    </div>

    <!--<div id="fh5co-started" class="fh5co-bg" style="background-image: url(images/img_bg_3.jpg);">
        <div class="overlay"></div>
        <div class="container">
            <div class="row animate-box">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <h2>Fitness Classes this Summer <br> <span> Pay Now and <br> Get <span class="percent">35%</span> Discount</span></h2>
                </div>
            </div>
            <div class="row animate-box">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <p><a href="#" class="btn btn-default btn-lg">Become a Member</a></p>
                </div>
            </div>
        </div>
    </div>-->


    <footer id="fh5co-footer" class="fh5co-bg" style="background-image: url(images/img_bg_1.jpg);" role="contentinfo">
        <div class="overlay"></div>
        <div class="container">
            <div class="row row-pb-md">
                <div class="col-md-12 text-center">
                    <h3>Contacto</h3>
                    <ul class="fh5co-footer-links">
                        <li><a href="#"><i class="icon-phone"></i> +52 1 (614) 216 37 85</a></li>
                        <li><a href="#"><i class="icon-phone"></i> +52 1 (614) 410 04 26</a></li>
                        <li><a href="#"><i class="icon-mail2"></i>info@hubstation.mx</a></li>
                        <li><a href="#"><i class="icon-mail2"></i>info@uhub.mx</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row copyright">
                <div class="col-md-12 text-center">
                    <h3>Social</h3>
                    <ul class="fh5co-social-icons">
                        <li><a href="#"><i class="icon-twitter"></i></a></li>
                        <li><a href="https://www.facebook.com/events/1381642831875722/"><i class="icon-facebook"></i></a></li>
                        <li><a href="#"><i class="icon-linkedin"></i></a></li>
                                    <!--<li><a href="#"><i class="icon-dribbble"></i></a></li>-->
                    </ul>
                </div>
            </div>
        </footer>
    </div>

    <div class="gototop js-top">
        <a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
    </div>
    <!-- jQuery -->
    <script src="{{asset('js/jquery.min.js')}}"></script>
    <script src="{{asset('sweetalert/sweetalert.min.js')}}"></script>
    <!-- jQuery Easing -->
    <script src="{{asset('js/jquery.easing.1.3.js')}}"></script>
    <!-- Bootstrap -->
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/bootstrap-switch.min.js')}}"></script>
    <!-- Waypoints -->
    <script src="{{asset('js/jquery.waypoints.min.js')}}"></script>
    <!-- Stellar Parallax -->
    <script src="{{asset('js/jquery.stellar.min.js')}}"></script>
    <!-- Carousel -->
    <script src="{{asset('js/owl.carousel.min.js')}}"></script>
    <!-- countTo -->
    <script src="{{asset('js/jquery.countTo.js')}}"></script>
    <!-- Magnific Popup -->
    <script src="{{asset('js/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{asset('js/magnific-popup-options.js')}}"></script>
    <!-- Google Map -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCefOgb1ZWqYtj7raVSmN4PL2WkTrc-KyA&sensor=false"></script>
    <script src="{{asset('js/google_map.js')}}"></script>
    <!-- Main -->
    <script src="{{asset('js/main.js')}}"></script>
    
    </body>
</html>

