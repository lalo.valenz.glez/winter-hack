<!DOCTYPE HTML>
<html>
    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Winter Hack</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,700,800" rel="stylesheet">
    
    <!-- Animate.css -->
    <link rel="stylesheet" href="{{asset('css/animate.css')}}">
    <!-- Icomoon Icon Fonts-->
    <link rel="stylesheet" href="{{asset('css/icomoon.css')}}">
    <!-- Bootstrap  -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap-switch.min.css')}}">
    <!-- Magnific Popup -->
    <link rel="stylesheet" href="{{asset('css/magnific-popup.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('sweetalert/sweetalert.css')}}">
    <!-- Owl Carousel  -->
    <link rel="stylesheet" href="{{asset('css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/owl.theme.default.min.css')}}">

    <!-- Theme style  -->
    <link rel="stylesheet" href="{{asset('css/style.css')}}">

    <!-- Modernizr JS -->
    <script src="{{asset('js/modernizr-2.6.2.min.js')}}"></script>
    <!-- FOR IE9 below -->
    <!--[if lt IE 9]>
    <script src="js/respond.min.js"></script>
    <![endif]-->

    </head>
    <body>
        
    <div class="fh5co-loader"></div>
    
    <div id="page">
    <nav class="fh5co-nav" role="navigation">
            <div class="top">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 text-right">
                            <p class="num">Call: +52 614 216 3785</p>
                            <ul class="fh5co-social">
                                <li><a href="https://www.facebook.com/events/1381642831875722/"><i class="icon-facebook"></i></a></li>
                                <li><a href="#"><i class="icon-dribbble"></i></a></li>
                                <li><a href="#"><i class="icon-github"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="top-menu">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 text-center">
                            <div id="fh5co-logo"><a href="">HackSpirits<span>.</span></a></div>
                        </div>
                        <div class="col-xs-8 text-right menu-1">
                            <ul>
                                <li><a href="">Inicio</a></li>
                                <li><a href="{{route('participantes.index')}}">Registrados</a></li>
                                <li><a href="{{route('workshop.index')}}">Workshop</a></li>
                            </ul>
                        </div>
                    </div>
                    
                </div>
            </div>
        </nav>
    <div id="fh5co-contact">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 animate-box">
                    <div class="fh5co-contact-info">
                        <h3>Participantes</h3>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>No. </th>
                                    <th>Nombre </th>
                                    <th>workshops</th>
                                </tr>
                                <tbody>
                                @foreach($participantes as $participante)
                                    <tr>
                                        <td>{{$participante->id}}</td>
                                        <td>{{$participante->first_name }} {{$participante->last_name}}</td>    
                                    </tr>
                                 @endforeach   
                                </tbody>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
                

    <!--<div id="fh5co-started" class="fh5co-bg" style="background-image: url(images/img_bg_3.jpg);">
        <div class="overlay"></div>
        <div class="container">
            <div class="row animate-box">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <h2>Fitness Classes this Summer <br> <span> Pay Now and <br> Get <span class="percent">35%</span> Discount</span></h2>
                </div>
            </div>
            <div class="row animate-box">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <p><a href="#" class="btn btn-default btn-lg">Become a Member</a></p>
                </div>
            </div>
        </div>
    </div>-->


    <footer id="fh5co-footer" class="fh5co-bg" style="background-image: url(images/img_bg_1.jpg);" role="contentinfo">
        <div class="overlay"></div>
        <div class="container">
            <div class="row row-pb-md">
                <div class="col-md-12 text-center">
                    <h3>Contacto</h3>
                    <ul class="fh5co-footer-links">
                        <li><a href="#"><i class="icon-phone"></i> +52 1 (614) 216 37 85</a></li>
                        <li><a href="#"><i class="icon-phone"></i> +52 1 (614) 410 04 26</a></li>
                        <li><a href="#"><i class="icon-mail2"></i>info@hubstation.mx</a></li>
                        <li><a href="#"><i class="icon-mail2"></i>info@uhub.mx</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row copyright">
                <div class="col-md-12 text-center">
                    <h3>Social</h3>
                    <ul class="fh5co-social-icons">
                        <li><a href="#"><i class="icon-twitter"></i></a></li>
                        <li><a href="https://www.facebook.com/events/1381642831875722/"><i class="icon-facebook"></i></a></li>
                        <li><a href="#"><i class="icon-linkedin"></i></a></li>
                                    <!--<li><a href="#"><i class="icon-dribbble"></i></a></li>-->
                    </ul>
                </div>
            </div>
        </footer>
    </div>

    <div class="gototop js-top">
        <a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
    </div>
    <!-- jQuery -->
    <script src="{{asset('js/jquery.min.js')}}"></script>
    <script src="{{asset('sweetalert/sweetalert.min.js')}}"></script>
    <!-- jQuery Easing -->
    <script src="{{asset('js/jquery.easing.1.3.js')}}"></script>
    <!-- Bootstrap -->
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/bootstrap-switch.min.js')}}"></script>
    <!-- Waypoints -->
    <script src="{{asset('js/jquery.waypoints.min.js')}}"></script>
    <!-- Stellar Parallax -->
    <script src="{{asset('js/jquery.stellar.min.js')}}"></script>
    <!-- Carousel -->
    <script src="{{asset('js/owl.carousel.min.js')}}"></script>
    <!-- countTo -->
    <script src="{{asset('js/jquery.countTo.js')}}"></script>
    <!-- Magnific Popup -->
    <script src="{{asset('js/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{asset('js/magnific-popup-options.js')}}"></script>
    <!-- Google Map -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCefOgb1ZWqYtj7raVSmN4PL2WkTrc-KyA&sensor=false"></script>
    <script src="{{asset('js/google_map.js')}}"></script>
    <!-- Main -->
    <script src="{{asset('js/main.js')}}"></script>
    <script>
        $(document).ready(()=>{
            var state_type = true;
            $('#radio_days').hide();
            var url = 'http://localhost:8000/register';
            //var url = 'http://104.236.237.212/register';
            $('#type_switch').attr('value', 'mentor');
            $('#type_switch').bootstrapSwitch({
                onText: 'Participante',
                offText: 'Mentor',
                animate: true,
                onSwitchChange: function(event, state){
                    if(state) { 
                        state_type = true;
                        $('#i-github').show()
                        $('#i-job_title').show()
                        $('#i-type_hacker').show()
                        $('#i-interested_hack').show();
                        $('#i-new_hack').show();
                        $('#radio_days').hide()
                    }else {
                        state_type = false;
                        $('#i-github').hide();
                        $('#i-job_title').hide();
                        $('#i-type_hacker').hide();
                        $('#i-new_hack').hide();
                        $('#i-interested_hack').hide();
                        $('#radio_days').show();
                    }
                    console.log(state_type); 
                }
            });

            $('#send').click(() => {
                if(state_type){
                    var data = {
                        _token: document.getElementsByName('_token')[0].value,
                        type: state_type ? 'participante' : 'mentor',
                        first_name: $('#first_name').val(), 
                        last_name: $('#last_name').val(),
                        github: $('#github').val(),
                        email: $('#email').val(),
                        job_title: $('#job_title').val(),
                        new_hack: $('input:radio[name=new_hack]:checked').val(), 
                        type_hacker: $('#type_hacker').val(),
                        interested_hack: $('#interested_hack').val(),
                    };
                    
                    $.post(url, data) 
                        .done((resp, status) => {
                            console.log(resp);
                            swal("Buen Trabajo", "Haz quedado inscrito en WinterHack", "success");
                        })
                        .fail((err) =>{
                            swal("Ups", "No Se pudo registrar", "warning");
                        });
                        console.log(data);
                }else{
                    var days_hack = '';
                    $('input[name="hack_days"]:checked').each(function() {
                        days_hack += $(this).val() + ",";
                    }); 
                    var data = {
                        _token: document.getElementsByName('_token')[0].value,
                        type: state_type ? 'participante' : 'mentor',
                        first_name: $('#first_name').val(), 
                        last_name: $('#last_name').val(),
                        email: $('#email').val(),
                        days: days_hack,
                    };
                    
                    $.post(url, data) 
                        .done((resp, status) => {
                            console.log(resp);
                            swal("Buen Trabajo", "Haz quedado inscrito en WinterHack", "success");
                        })
                        .fail((err) =>{
                            swal("Ups", "No Se pudo registrar", "warning");
                        });
                    console.log(data);
                }


            });
        });
    </script>
    </body>
</html>

