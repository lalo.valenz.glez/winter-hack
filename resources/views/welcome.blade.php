<!DOCTYPE HTML>
<html>
    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Winter Hack</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,700,800" rel="stylesheet">
    
    <!-- Animate.css -->
    <link rel="stylesheet" href="{{asset('landing/css/animate.css')}}">
    <!-- Icomoon Icon Fonts-->
    <link rel="stylesheet" href="{{asset('landing/css/icomoon.css')}}">
    <!-- Bootstrap  -->
    <link rel="stylesheet" href="{{asset('landing/css/bootstrap.css')}}">

    <!-- Magnific Popup -->
    <link rel="stylesheet" href="{{asset('landing/css/magnific-popup.css')}}">

    <!-- Owl Carousel  -->
    <link rel="stylesheet" href="{{asset('landing/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('landing/css/owl.theme.default.min.css')}}">

    <!-- Theme style  -->
    <link rel="stylesheet" href="{{asset('landing/css/style.css')}}">

    <!-- Modernizr JS -->
    <script src="{{asset('landing/js/modernizr-2.6.2.min.js')}}"></script>
    <!-- FOR IE9 below -->
    <!--[if lt IE 9]>
    <script src="js/respond.min.js"></script>
    <![endif]-->

    </head>
    <body>
        
        <div class="gtco-loader"></div>
        <div id="page">
            <!-- <div class="page-inner"> -->
            <nav class="gtco-nav" role="navigation">
                <div class="gtco-container">
                    <div class="row">
                        <div class="col-sm-4 col-xs-12">
                            <div id="gtco-logo"><a href="">Hack <em>Spirit</em></a></div>
                        </div>
                        <div class="text-right menu-1">
                            <ul>
                                <li><a href="https://docs.google.com/presentation/d/1niKbIhtUGfY2YdUr6ZeSbAT1QnDJ2sa01iX2PCwaIqg/htmlpresent">Guia!</a></li>
                                <li class="btn-cta"><a href="{{route('register.index')}}"><span>REGISTRATE!</span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
            <header id="gtco-header" class="gtco-cover gtco-cover-md" role="banner" style="background-image: url(images/logo.png)" data-stellar-background-ratio="0.8">
                <div class="overlay"></div>
                <div class="gtco-container">
                    <div class="row row-mt-15em">
                        <div class="hidden-xs col-md-12 col-md-offset-0 text-left">
                            <div class="col-md-8 col-md-push-2 animate-box" data-animate-effect="fadeInRight">
                                <div class="form-wrap">
                                    <div class="tab">
                                        <div class="tab-content text-center">
                                            <h2 style="font-weight: 600; color: #15cbcc">Un fin de semana para crear soluciones tecnológicas a los problemas de la comunidad. <br> ¡Aprende, comparte y diviértete!</h2>
                                            <table>
                                                <td>
                                                    <a href="https://docs.google.com/presentation/d/1niKbIhtUGfY2YdUr6ZeSbAT1QnDJ2sa01iX2PCwaIqg/htmlpresent"><img style="max-width: 150px; margin-left: 5em" src="images/logoguide.png" alt=""></a>
                                                </td>
                                                <td>
                                                    <a href="{{route('register.index')}}"><img style="max-width: 200px; margin-left: 10em" src="images/logoreg.png" alt=""></a>
                                                </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        <div class="col-xs-8 col-xs-offset-2  visible-xs text-left">
            <div class="row row-mt-15em"> <img id="xsLogo" src="images/logow.png" alt=""> </div>
        </div>
        </div>
        </div>
        </header>
        <div class="gtco-section" id="Products">
            <div class="gtco-container">
                <div class="row">
                    <div class="col-md-12 text-center gtco-heading">
                        <h2 class="cursive-font primary-color">Colaboradores</h2>
                        <legend></legend>
                        <div class="row ">
                            <div class="col-md-4 col-lg-4 col-sm-8 col-xs-8 brandlogo">
                                <img class="img-responsive " src="images/logouh.png" alt="">
                            </div>
                            <div class="col-md-4 col-lg-4 col-sm-8 col-xs-8 brandlogo">
                                <img class="img-responsive " src="images/logonf.jpeg" alt="">
                            </div>
                            <div class="col-md-4 col-lg-4 col-sm-8 col-xs-8 brandlogo">
                                <img style="" class=" img-responsive " src="images/logoei.jpg" alt="">
                            </div>
                            <legend></legend>
                            <div class="row">
                                <div class="col-md-4 col-lg-4 col-sm-8 col-xs-8 brandlogo">
                                    
                                </div>
                                <div class="col-md-4 col-lg-4 col-sm-8 col-xs-8 brandlogo">
                                    <img class="img-responsive " src="images/logocc.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer id="gtco-footer" role="contentinfo" style="background-image: url(images/img_bg_1.jpg)" data-stellar-background-ratio="0.5">
                <div class="overlay"></div>
                <div class="gtco-container">
                    <div class="row row-pb-md">
                        <div class="col-md-12 text-center">
                            <div class="gtco-widget">
                                <h3>Contacto</h3>
                                <ul class="gtco-quick-contact">
                                    <li><a href="#"><i class="icon-phone"></i> +52 1 (614) 216 37 85</a></li>
                                    <li><a href="#"><i class="icon-phone"></i> +52 1 (614) 410 04 26</a></li>
                                    <li><a href="#"><i class="icon-mail2"></i>info@hubstation.mx</a></li>
                                    <li><a href="#"><i class="icon-mail2"></i>info@uhub.mx</a></li>
                                    <!--<li><a href="#"><i class="icon-chat"></i> Live Chat</a></li>-->
                                </ul>
                            </div>
                            <div class="gtco-widget">
                                <h3>Social</h3>
                                <ul class="gtco-social-icons">
                                    <li><a href="#"><i class="icon-twitter"></i></a></li>
                                    <li><a href="https://www.facebook.com/events/1381642831875722/"><i class="icon-facebook"></i></a></li>
                                    <li><a href="#"><i class="icon-linkedin"></i></a></li>
                                    <!--<li><a href="#"><i class="icon-dribbble"></i></a></li>-->
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            <!-- </div> -->
        </div>
        <div class="gototop js-top"> <a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a> </div>
        <!-- jQuery -->
        <script src="{{asset('landing/js/jquery.min.js')}}"></script>
        <!-- jQuery Easing -->
        <script src="{{asset('landing/js/jquery.easing.1.3.js')}}"></script>
        <!-- Bootstrap -->
        <script src="{{asset('landing/js/bootstrap.min.js')}}"></script>
        <!-- Waypoints -->
        <script src="{{asset('landing/js/jquery.waypoints.min.js')}}"></script>
        <!-- Stellar Parallax -->
        <script src="{{asset('landing/js/jquery.stellar.min.js')}}"></script>
        <!-- Carousel -->
        <script src="{{asset('landing/js/owl.carousel.min.js')}}"></script>
        <!-- countTo -->
        <script src="{{asset('landing/js/jquery.countTo.js')}}"></script>
        <!-- Magnific Popup -->
        <script src="{{asset('landing/js/jquery.magnific-popup.min.js')}}"></script>
        <script src="{{asset('landing/js/magnific-popup-options.js')}}"></script>
        <!-- Google Map -->
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCefOgb1ZWqYtj7raVSmN4PL2WkTrc-KyA&sensor=false"></script>
        <script src="{{asset('landing/js/google_map.js')}}"></script>
        <script type="text/javascript">
            $(function() {
                $('a[href*="#"]:not([href="#"])').click(function() {
                    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                        var target = $(this.hash);
                        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                        if (target.length) {
                            $('html, body').animate({
                                scrollTop: target.offset().top
                            }, 1000);
                            return false;
                        }
                    }
                });
            });
        </script>
        <!-- Main -->
        <script src="{{asset('landing/js/main.js')}}"></script>

    </body>
</html>

