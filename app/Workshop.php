<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Workshop extends Model
{
    protected $table = 'workshops';

    protected $fillable = ['id', 'title', 'author'];

    public function users()
    {
        return $this->belongsToMany('App\User');
    }
}
