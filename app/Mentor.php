<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mentor extends Model
{
    protected $table = 'mentors';

    protected $fillable = ['id', 'first_name', 'last_name', 'email', 'days'];
}
